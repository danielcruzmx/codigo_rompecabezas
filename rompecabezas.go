// CODIGO EN GO

package main

import ( "fmt" 
	     "strings"
	     "math"
)

// ESTRUCTURAS DE DATOS

type Puzzle struct{
	estado      string
	valor       int
	explorado   int
	nivel       int 
	ascendiente string
}

type Node struct{
	dato *Puzzle
	prev *Node
	next *Node
}

type List struct{
	head *Node
	tail *Node
	best *Node
}

type Stack []string

// METODOS

// AGREGA NUEVO NODO A LA LISTA
func (l *List) Agrega(nuevoNodo *Node) {
	if l.head == nil {
	   l.head = nuevoNodo
	   l.tail = nuevoNodo
	   l.best = nuevoNodo
	} else {
       nuevoNodo.prev = l.best
	   nuevoNodo.next = nil
	   l.tail.next    = nuevoNodo
	   l.tail         = nuevoNodo
	}
}

// MUEVE APUNTADOR -BEST-
// A MEJOR NODO DE LA LISTA
func (l *List) ObtieneMejor() {
	maximo := 2000
	maxima := 2000
	valor  := 0

	auxNodo := l.head
	// MEJOR VALOR DE LA LISTA
	for auxNodo != nil {
		valor = auxNodo.dato.valor + auxNodo.dato.nivel
		if(auxNodo.dato.explorado == 0 && valor < maximo){
			maximo = valor
		}
		auxNodo = auxNodo.next
	} 

	auxNodo = l.head
	// MEJOR VALOR CON MEJOR HEURISTICA
	for auxNodo != nil {
		valor = auxNodo.dato.valor + auxNodo.dato.nivel
		heuristica := auxNodo.dato.valor
		if(auxNodo.dato.explorado == 0 && 
					   valor == maximo &&
					heuristica < maxima){
			// MUEVE APUNTADOR			
			l.best = auxNodo
			maxima = heuristica
		}
		auxNodo = auxNodo.next
	}	

	l.best.dato.explorado = 1
}

// METODOS DEL STACK
// PRIMERO EN ENTRAR
// ULTIMO EN SALIR
func (s *Stack) Vacia() bool {
	return len(*s) == 0
}

func (s *Stack) Push(str string){
	*s = append(*s, str)
}

func (s *Stack) Pop() (string, bool){
	if s.Vacia(){
		return "", false
	} else {
		index   := len(*s) - 1
		element := (*s)[index]
		*s = (*s)[:index]
		return element, true  
	}
}

// GUARDA LOS MEJORES NODOS EN UN STACK 
// RECORRE DESDE EL ULTIMO MEJOR NODO
// O SOLUCION HASTA EL PRIMER NODO 
// POR EL APUNTADOR -PREV-
func (l *List) Mejores() Stack{
	auxNodo := l.best
	var stack Stack
	for auxNodo != nil{
		stack.Push(auxNodo.dato.estado)
		auxNodo = auxNodo.prev
	}
	return stack
}

// OBTIENE LOS DESCENDIENTES 
func (e *Puzzle) Descendientes() []string {
	var pVacio, pDes, j  int
	var aux, pza         string
	var buffer []string
	// TABLA DE INTERCAMBIOS
	movtosValidos := [][]int{ []int{1,3,9,9}, []int{0,2,4,9},  []int{1,5,9,9},
							  []int{0,4,6,9}, []int{1,3,5,7,9},[]int {2,4,8,9},
							  []int{3,7,9,9}, []int{4,6,8,9},  []int {5,7,9,9},
							}
	j      = 0						
	pVacio = strings.Index(e.estado, " ") // POSICION DEL ESPACIO
	pDes   = movtosValidos[pVacio][j]     // NUEVA POSICION
    for pDes != 9 {
		pza  = string(e.estado[pDes])
		aux  = strings.Replace(e.estado, pza, "*", 1)
		aux  = strings.Replace(aux     , " ", pza, 1)
		aux  = strings.Replace(aux     , "*", " ", 1)
		j    = j + 1
		pDes = movtosValidos[pVacio][j]
		// GUARDA DESCENDIENTE EN BUFFER
		buffer = append(buffer, aux)
	}	
	return buffer
}

// FUNCIONES

// CALCULA LA DISTANCIA MANHATTAN DE UN ESTADO
func distanciaManhattan(estado string, meta string) int {
	pzas := "12345678"
	suma := 0.0
	for _, n := range pzas {
		x1 := posX(estado, string(n))
		y1 := posY(estado, string(n))
		x2 := posX(meta,   string(n))
		y2 := posY(meta,   string(n))
		dm := math.Abs(float64(x1 - x2)) + 
			  math.Abs(float64(y1 - y2))
		suma = suma + dm
	} 
	return int(suma)
}

func posX(cadena string, caracter string) int {
	 coorX := []int{1,1,1,2,2,2,3,3,3}
	 pos   := strings.Index(cadena, caracter)
	 ret   := coorX[pos] 
     return ret
}

func posY(cadena string, caracter string) int {
	 coorY := []int{1,2,3,1,2,3,1,2,3}
	 pos   := strings.Index(cadena, caracter)
	 ret   := coorY[pos] 
	 return ret
}

func imprime(estado string, valor int, nivel int){
	fmt.Printf("\n")
	fmt.Printf("|%c|%c|%c|\n", estado[0], estado[1], estado[2])
	fmt.Printf("|%c|%c|%c| -> %d %d\n", estado[3], estado[4], estado[5], valor, nivel)
	fmt.Printf("|%c|%c|%c|\n", estado[6], estado[7], estado[8])
	fmt.Printf("\n")
}

// PRINCIPAL
func main() {
	
	var i         int
	var ciclos    int
	var valor     int
	var nivel     int
	var explorado int
	var padre     string
	var estado    string
	var edoIni    string
	var edoFin    string
	var stack     Stack 

	edoIni    = "4531 2678"
	edoFin    = "12345678 "
	valor     = distanciaManhattan(edoIni, edoFin)
	explorado = 0
	padre     = ""
	estado    = ""

	lista   := &List{}
	
	nodoInicial := Node {
				 dato: &Puzzle{estado: edoIni, 
								valor: valor, 
								nivel: nivel, 
							explorado: explorado,
					  	  ascendiente: estado},
				 prev: nil,
				 next: nil,
	} 

	lista.Agrega(&nodoInicial)
	lista.ObtieneMejor()

	// CICLO DE SOLUCION
	ciclos = 2000
		 i = 0
	// 2000 CICLOS O HASTA ENCONTRAR SOLUCION	 
	for	i < ciclos && lista.best.dato.estado != edoFin {
		// DATOS DEL MEJOR NODO
		padre  = lista.best.dato.ascendiente
		nivel  = lista.best.dato.nivel 
		estado = lista.best.dato.estado
		// GENERA DESCENDIENTES
		for _, hijo := range lista.best.dato.Descendientes(){
			valor     = distanciaManhattan(hijo, edoFin)
			explorado = 0
			
			auxNodo := Node {
				dato: &Puzzle{estado: hijo, 
							   valor: valor, 
							   nivel: nivel + 1, 
					   	   explorado: explorado, 
						 ascendiente: estado},
				prev: nil,
				next: nil,
			}
			
			// AGREGA A LA LISTA 
			if( padre != hijo ){
				lista.Agrega(&auxNodo)
			}
		}	

		// VUELVE A OBTENER EL MEJOR
		lista.ObtieneMejor()
		i = i + 1 
	}

	// RECUPERA LOS MEJORES NODOS
	// EN UN STACK 
	stack = lista.Mejores()

	// IMPRIME SOLUCION DESDE 
	// ESTADO INCIAL HASTA ESTADO META	
	for len(stack) > 0 {
		x, y := stack.Pop()
		if y == true {
			imprime(x,0,0)
		}
	}
	
}
