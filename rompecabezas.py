# CODIGO PYTHON

# NODO
class Node(object):
    def __init__(self, data, prev, next):
        self.data = data
        self.prev = prev
        self.next = next

# LISTA DOBLEMENTE ENLAZADA
class DoubleList(object):
    head = None
    tail = None
    best = None
    explorado = 1
 
    def append(self, data):
        newNode = Node(data, None, None)
        if self.head is None:
            self.head = self.tail = self.best = newNode
        else:
            newNode.prev = self.best
            newNode.next = None
            self.tail.next = newNode
            self.tail = newNode
        
    def recover(self):
        currentNode = self.best
        while currentNode is not None:
            yield currentNode.data
            currentNode = currentNode.prev

    def obtBestNode(self):
        f  = 0
        m  = 2000
        h  = 0
        nm = m

        i = self.head
        while i is not None:
           f = i.data.h + i.data.g
           if(i.data.explorado==0 and f < m):
              m = f
           i = i.next

        i = self.head
        while i is not None:
           f = i.data.h + i.data.g
           h = i.data.h
           if(i.data.explorado==0 and f==m and h < nm):
              self.best = i
              nm = h
           i = i.next
        
        self.explorado = self.explorado + 1
        self.best.data.explorado = self.explorado
  

# ESCENARIO 
class Puzzle(object):
  coordenadas_x = [1,1,1,2,2,2,3,3,3]
  coordenadas_y = [1,2,3,1,2,3,1,2,3]
  elementos     = ["1","2","3","4","5","6","7","8"]
  intercambios  = [[1,3,9,9],
                   [0,2,4,9],
                   [1,5,9,9],
                   [0,4,6,9],
                   [1,3,5,7,9],
                   [2,4,8,9],
                   [3,7,9,9],
                   [4,6,8,9],
                   [5,7,9,9]]

  def __init__(self, estado, meta, nivel, explorado, ascendiente):
    self.estado = estado
    #self.meta = meta
    self.h = self.distanciaManhatan(meta)
    self.g = nivel
    self.explorado = explorado
    self.ascendiente = ascendiente

  def posx(self, c, cadena):
    return self.coordenadas_x[cadena.index(c)]

  def posy(self, c, cadena):
    return self.coordenadas_y[cadena.index(c)]

  def distanciaManhatan(self, meta):
    suma = 0
    for j in self.elementos:
      dmx = abs(self.posx(j, self.estado) - self.posx(j, meta))
      dmy = abs(self.posy(j, self.estado) - self.posy(j, meta))
      suma = suma + dmx + dmy
    return suma

  def lstDescendientes(self):
    b = self.estado.index(" ")
    a = self.ascendiente
    r = list()
    j = 0
    while(self.intercambios[b][j] != 9):
      d = self.estado[:]
      d[b] = self.estado[self.intercambios[b][j]]
      d[self.intercambios[b][j]] = " "
      j = j + 1
      if( d != a ):
        r.append(d)
    return r

  def imprime(self):
    print("_____")
    print(' '.join(self.estado[0:3]))
    print(' '.join(self.estado[3:6]))
    print(' '.join(self.estado[6:9]))
    print("_____ h->{}  n->{} f->{} e->{}".format(self.h, self.g, self.h + self.g, self.explorado))
    print('')


# INICIO
if __name__ == "__main__":

  auxiliar = []
  edoIni   = list("4531 2678")
  edoMeta  = list("12345678 ")
  
  # LISTA 
  lista = DoubleList()
  
  # PUZZLES
  puzzleIni  = Puzzle(edoIni,  edoMeta, 0, 1, None)
  puzzleMeta = Puzzle(edoMeta, edoMeta, 0, 1, None)

  # IMPRIME ENCABEZADOS DEL JUEGO
  print("Rompecabezas de 8 piezas")
  print("")
  print("Estado inicial")
  puzzleIni.imprime()
  print("Estado meta ")
  puzzleMeta.imprime()

  print("Inicia solucion ")
 
  # METE A LA LISTA EL NODO INICIAL
  lista.append(puzzleIni)
  # MUEVE APUNTADOR A MEJOR NODO
  lista.obtBestNode()
  # CONSERVA DATOS DEL MEJOR NODO
  mejorPuzzle =  lista.best.data

  j = 0
  # HASTA QUE SE CONSIGA EL ESTADO META 
  # O EL NUMERO DE CICLOS SEA MENOR A 2000
  while (j < 2000 and mejorPuzzle.estado != puzzleMeta.estado):
    # OBTIENE DESCENDIENTES DEL MEJOR NODO 
    # Y LOS AGREGA A LA LISTA
    for hijo in mejorPuzzle.lstDescendientes():
      puzzle = Puzzle(hijo, edoMeta, mejorPuzzle.g + 1, 0, mejorPuzzle.estado)
      lista.append(puzzle)
    
    # VUELVE A MOVER APUNTADOR A MEJOR NODO  
    lista.obtBestNode()
    # CONSERVA DATOS DEL NUEVO MEJOR NODO
    mejorPuzzle =  lista.best.data
    # SIGUIENTE CICLO
    j = j + 1

  # RECUPERA LISTA DESDE EL ULTIMO MEJOR NODO 
  # HASTA EL ESTADO INICIAL
  for v in lista.recover():
      auxiliar.append(v)

  # INVIERTE LISTA RECUPERADA
  auxiliar.reverse()
  
  # IMPRIME SOLUCION DESDE EL ESTADO INICIAL 
  # HASTA EL ULTIMO MEJOR NODO O META
  for puzzle in auxiliar:
      puzzle.imprime()
  
