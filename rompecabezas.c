/* Solucion del rompecabezas de 8 piezas */
/* Visual C++ Estructurado               */

#include "stdafx.h"
#include <set>
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

struct Tnodo
{
  string edo;
  int h;
  int g;
  char mca;
  Tnodo *sig;
  Tnodo *padre;
};  

typedef struct Tnodo Tlista;

void pinta_escenario(Tnodo n);
void pinta_estado(string s);
void agrega_nodo(string s, string m);
void genera_descendientes(string m, string p);
void recupera_mejores();
void obtiene_mejor_nodo();
int pos(string s, char cs);
int pos_x(char c, string s);
int pos_y(char c, string s);
int distancia_manhatan(string s, string m);

Tlista *ppio,*ultimo,*mejor ;

string edo_i("12345678 ");
string edo_m("453172 68");

int total_nodos;

int main() {
    Tlista *nodo;
    int j=0;
    cout << "Rompecabezas de 8 piezas" << endl ;
    cout << endl << " Partimos del estado " << endl;
    pinta_estado(edo_m);
    cout << endl << " hasta llegar al estado " << endl;
    pinta_estado(edo_i);
    cout << endl  << " Solucion  " << endl;
    agrega_nodo(edo_i,edo_m);
    while(mejor->h!=0){
        obtiene_mejor_nodo();
        nodo = mejor->padre;
        if(nodo != NULL) {
            genera_descendientes(mejor->edo,mejor->padre->edo);
        } else {
            genera_descendientes(mejor->edo,"0");
        }
        j++;
    }
    cout << "-> Total de ciclos = " << j << endl;
    cout << "-> Total de nodos  = " <<    total_nodos << endl << endl;
    recupera_mejores();
    getchar();
    return 0;
}

void pinta_escenario(Tnodo nodo) {
    cout << "->Estado --------" << endl;
    pinta_estado(nodo.edo);
    cout << " h = " << nodo.h << " g = " << nodo.g << " f = " << nodo.g + nodo.h << endl << endl;
}

void pinta_estado(string edo) {
    cout << edo[0] << edo[1] << edo[2] << endl;
    cout << edo[3] << edo[4] << edo[5] << endl;
    cout << edo[6] << edo[7] << edo[8] << endl;
}

void recupera_mejores(){
  Tlista *nodo;
  nodo = mejor;
  while(nodo){
      pinta_escenario(*nodo);    
      nodo = nodo->padre;
  }  
}  

void agrega_nodo(string s, string m){
  Tlista  *nodo = new Tlista;
  if(nodo==NULL){
    cout << "ERROR" << endl;
  }
  else {
    nodo->edo = string(s);
    nodo->sig = NULL;
    if(ppio == NULL){
        nodo->mca = '1';    
        nodo->g = 0;
        nodo->h = distancia_manhatan(s,m);
        nodo->padre = NULL;
        ppio = nodo;
        ultimo = nodo;
        mejor = nodo;
    } else {  
        nodo->mca = '0';    
        nodo->g = mejor->g+1;
        nodo->h = distancia_manhatan(s,m);
        nodo->padre = mejor;
        ultimo->sig = nodo;
        ultimo = nodo;
    }
    total_nodos+=1;    
  }
}

int distancia_manhatan(string escenario, string meta){
  int suma = 0;
  int x1,x2,y1,y2;
  int dm = 0;
  for(char j=49; j<57; j++){
     x1 = pos_x(j,escenario);    
     y1 = pos_y(j,escenario);    
     x2 = pos_x(j,meta);    
     y2 = pos_y(j,meta);    
     dm = abs(x1-x2) + abs(y1-y2);
     suma = suma + dm;
  }
  return suma;
}  

int pos_x(char pieza, string escenario){
  int v=0;
  int coordenadas_x[] = {1,1,1,2,2,2,3,3,3};
  int p = pos(escenario, pieza);
  v = coordenadas_x[p];
  return v;
}

int pos_y(char pieza, string escenario){
  int v=0;
  int coordenadas_y[] = {1,2,3,1,2,3,1,2,3};
  int p = pos(escenario, pieza);
  v = coordenadas_y[p];
  return v;
}

void obtiene_mejor_nodo(){
  int f = 0;
  int m = 2000;
  int h = 0;
  int nm = m;
  Tlista *nodo;
  nodo = ppio;
  while(nodo){
    f = nodo->h + nodo->g;
    if(nodo->mca == '0' && f < m){
      m = f;
    }
    nodo = nodo->sig;
  }  
  Tlista *nod;
  nod = ppio;
  while(nod){
    f = nod->h + nod->g;
    h = nod->h;
    if(nod->mca == '0' && f == m && h < nm){
      mejor = nod;
      nm = h;
    }
    nod = nod->sig;
  }  
  mejor->mca = '1';
}

void genera_descendientes(string mejor, string padre){
  int c[5];
  int b;
  int j;
  string q;
  b = pos(mejor,' ');
  q = string(mejor);
  switch(b){
     case 0:  c[0] = 1; c[1] = 3; c[2] = 9; c[3] = 9; break;
     case 1:  c[0] = 0; c[1] = 2; c[2] = 4; c[3] = 9; break;
     case 2:  c[0] = 1; c[1] = 5; c[2] = 9; c[3] = 9; break;
     case 3:  c[0] = 0; c[1] = 4; c[2] = 6; c[3] = 9; break;
     case 4:  c[0] = 1; c[1] = 3; c[2] = 5; c[3] = 7; c[4] = 9; break;
     case 5:  c[0] = 2; c[1] = 4; c[2] = 8; c[3] = 9; break;
     case 6:  c[0] = 3; c[1] = 7; c[2] = 9; c[3] = 9; break;
     case 7:  c[0] = 4; c[1] = 6; c[2] = 8; c[3] = 9; break;
     case 8:  c[0] = 5; c[1] = 7; c[2] = 9; c[3] = 9; break;
  }
  for(j=0; c[j] != 9; j++){
    q[b] = mejor[c[j]];
    q[c[j]] = ' ';
    if(padre.compare(q)) {  
      agrega_nodo(q, edo_m);
    }
    q = string(mejor);
  }  
}  

int pos(string s, char ca) {
  int c=0;
  while(s[c]!=ca)c++;
  return c;  
}  
