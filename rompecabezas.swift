# MAIN
# Codigo SWIFT 

var estadoInicial = Puzzle(estado: ["4","5","3","1"," ","2","6","7","8"], g:0, e:0, ascend: [] )
var estadoFinal   = Puzzle(estado: ["1","2","3","4","5","6","7","8"," "], g:0, e:0, ascend: [] )
var ciclos: Int   = 2000

var es = Escenario(estadoInicial: estadoInicial, estadoFinal: estadoFinal)
var re = es.solucion(ciclos)

if (re > 0){
    println("Encontre la solucion en \(re) ciclos")
    es.imprimeSolucion()
} else {
    println("No se encontro solucion en \(ciclos) ciclos")
}

# NODO
class Nodo {

    var datos: T?        = nil
    var siguiente: Nodo? = nil
    var previo: Nodo?    = nil
    
    init() {
    }
    
    init(valor: T) {
        self.datos = valor
    }
    
}

# LISTA
class ListaDoble {
    
    var count: Int = 0
    var head: Nodo?
    var tail: Nodo?
    var best: Nodo?

    init() {
    }
    
    func isEmpty() -> Bool {
        return self.count == 0
    }
    
    func agrega(datos: T) {
        
        var nodo = Nodo(valor: datos)
        
        if self.isEmpty() {
            self.head = nodo
            self.tail = nodo
            self.best = nodo
        } else {
            nodo.previo = self.best
            self.tail?.siguiente = nodo
            self.tail = nodo
        }
        
        self.count++
    }
}

# PUZZLE
class Puzzle {
    
    let coordenadas_x = [1,1,1,2,2,2,3,3,3]
    let coordenadas_y = [1,2,3,1,2,3,1,2,3]
    let elementos     = ["1","2","3","4","5","6","7","8"]
    let intercambios  = [[1,3,9,9],[0,2,4,9]  ,[1,5,9,9],
                         [0,4,6,9],[1,3,5,7,9],[2,4,8,9],
                         [3,7,9,9],[4,6,8,9]  ,[5,7,9,9]]
    
    var estado: [String] = []
    var ascend: [String] = []
    var h: Int           = 0
    var g: Int           = 0
    var e: Int           = 0
    
    init(estado: [String], g:Int, e:Int, ascend: [String]) {
        
        self.estado = estado
        self.h = 0
        self.g = g
        self.e = e
        self.ascend = ascend
    }
    
    func pos(c: String) -> Int{
        
        for (index, value) in enumerate(self.estado) {
            if(value == c){ return index }
        }
        return 0
    }
    
    func posx(c: String) -> Int{
        
        return self.coordenadas_x[self.pos(c)]
    }
    
    func posy(c: String) -> Int{
        
        return self.coordenadas_y[self.pos(c)]
    }
    
    func distanciaManhatan(meta: Puzzle) -> Int{
        
        var suma: Int = 0
        var dmx:  Int = 0
        var dmy:  Int = 0
        
        for (index, value) in enumerate(self.elementos) {
            dmx = abs(self.posx(value) - meta.posx(value))
            dmy = abs(self.posy(value) - meta.posy(value))
            suma = suma + dmx + dmy
        }
        return suma
    }
    
    func obtDescendientes() -> [[String]]{
        
        var b :Int = self.pos(" ")
        var j :Int = 0
        var r :[[String]] = []
        
        let a = self.ascend
        
        while(self.intercambios[b][j] != 9 ){
            var d: [String] = self.estado
            d[b] = self.estado[self.intercambios[b][j]]
            d[self.intercambios[b][j]] = " "
            if( d != a ){
                r.append(d)
            }
            j = j + 1
        }
        return r
    }
    
    func printPuzzle(){
        
        println(self.estado[0]+self.estado[1]+self.estado[2])
        println(self.estado[3]+self.estado[4]+self.estado[5])
        println(self.estado[6]+self.estado[7]+self.estado[8])
        println("-----")
    }
}

# ESCENARIO
class Escenario {

    var lista = ListaDoble()
    
    var estadoInicial: Puzzle
    var estadoFinal:   Puzzle
    
    
    init(estadoInicial: Puzzle, estadoFinal: Puzzle) {
        
        self.estadoInicial = estadoInicial
        self.estadoFinal   = estadoFinal
        estadoInicial.h = estadoInicial.distanciaManhatan(estadoFinal)
        lista.agrega(estadoInicial)
    }
    
    func solucion(ciclos: Int) -> Int {
    
        var j:Int = 0
        var mejorNodo = mejorNodoLista()
        
        while( j < ciclos ){
            let ctos  = mejorNodo?.datos?.obtDescendientes().count
            let edoMn = mejorNodo?.datos?.estado
            let g     = mejorNodo?.datos?.g
            
            if(edoMn! == estadoFinal.estado){
                break;
            }
            
            var hijo:Int = 0
            while(hijo < ctos){
                let edo = mejorNodo?.datos?.obtDescendientes()[hijo]
                var e = Puzzle(estado: edo!, g:g! + 1, e:0, ascend:edoMn!)
                e.h = e.distanciaManhatan(estadoFinal)
                lista.agrega(e)
                hijo = hijo + 1
            }
            mejorNodo = mejorNodoLista()
            j = j + 1
        }
        return j
    }
 
    func mejorNodoLista() ->  Nodo?{
    
        var  m = 2000
        var nm = 2000
        var i = lista.head
    
        while(i?.datos  != nil){
            let h = i?.datos?.h
            let g = i?.datos?.g
            let f = h! + g!
            let e = i?.datos?.e
    
            if( e==0 && f < m){
                m = f
            }
            i = i?.siguiente
        }
    
        i = lista.head
    
        while(i?.datos != nil){
            let h = i?.datos?.h
            let g = i?.datos?.g
            let f = h! + g!
            let e = i?.datos?.e
    
            if( e==0 && f == m && h < nm){
                lista.best = i
                nm = h!
            }
            i = i?.siguiente
        }
        lista.best?.datos?.e = 1
        return lista.best
    }

    func imprimeSolucion() {
    
        var PuzzleStack = Stack()
        var rec = lista.best
    
        while(rec?.datos != nil){
            let ed = rec?.datos
            PuzzleStack.push(ed!)
            rec = rec?.previo
        }
    
        while(PuzzleStack.items.count > 0) {
            PuzzleStack.pop().printPuzzle()
        }
    }
}

# STACK
struct Stack{
    
    var items = [ContainedType]()
    
    mutating func push(item: ContainedType) {
        items.append(item)
    }
    
    mutating func pop() -> ContainedType {
        return items.removeLast()
    }
}




